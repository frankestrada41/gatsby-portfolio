import * as React from 'react'
import Gallery from 'react-photo-gallery'

const photos = [
    {
        src: 'https://cdn.pixabay.com/index/2021/06/17/14-18-10-796_1440x550.jpg',
        width: 4,
        height: 3,
    }
]

const AboutGallery = () => {
    return (
        <Gallery photos={photos} />
    )
}

export default AboutGallery